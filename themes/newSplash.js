/* jshint node: true, multistr: true, trailing: true, esnext: true */
module.exports = {
  version: "0.0.5",

  // emit headers
  header: function($, opt) {
    var p = opt["--urlprefix"] + "/";
    $('head link[href*="wikieducator.org/load.php"]').remove();
    $('head link[rel="shortcut icon"]').attr("href", p + "img/favicon.png");
    $("head").append(
      '\
  <!-- Bootstrap -->\n\
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,200,700" rel="stylesheet" type="text/css">\n\
  <link rel="stylesheet" type="text/css" href="' +
        p +
        'css/bootstrap.css">\n\
  <link rel="stylesheet" type="text/css" href="' +
        p +
        'css/layout.css">\n\
  <link rel="stylesheet" type="text/css" href="' +
        p +
        'css/typography.css">\n\
  <link rel="stylesheet" type="text/css" href="' +
        p +
        'css/colours.css">\n\
  <link rel="stylesheet" type="text/css" href="' +
        p +
        'css/we.css">\n\
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->\n\
  <!-- WARNING: Respond.js does not work if you view the page via file:// -->\n\
  <!--[if lt IE 9]>\n\
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>\n\
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>\n\
  <![endif]-->\n\
  <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->\n\
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>\n\
  <!-- Include all compiled plugins (below), or include individual files as needed -->\n\
  <script src="' +
        p +
        'js/bootstrap.min.js"></script>\n\
  \n\
  <script src="' +
        p +
        'js/we.js"></script>\n'
    );
  },

  footer: function($, opt, pages, pi) {
    var p = opt["--urlprefix"] + "/img/";
    $(".printfooter").remove();
    $("#footer")
      .addClass("container")
      .prepend(
        '<div class="ilogo"><a href="' +
          opt["--link"] +
          '"><img src="' +
          opt["--urlprefix"] +
          "/img/" +
          opt["--logo"] +
          '" alt="institution logo"></a></div>'
      );
    /*
    $('#footer-places').append('<li><a href="http://wikieducator.org' + pages[pi] + '?action=history">Authors</a></li>');
    // move the license icon
    $('#footer-copyrightico').detach()
      .appendTo('#footer-places');
    $('#footer-icons').remove();
    */
    $("#footer").after(
      '<footer>\n\
  <div class="container">\n\
    <div class="row">\n\
      <div class="col-md-6">\n\
        <div class="footertext">Content is available under the \n\
        <a rel="license" href="http://WikiEducator.org/WikiEducator:Copyrights" title="WikiEducator:Copyrights">Creative Commons Attribution Share Alike License</a>.\n\
        </div>\n\
        <div class="footertext"><a href="http://wikieducator.org/WikiEducator:Privacy_policy">Privacy Policy</a> | <a href="http://wikieducator.org' +
        pages[pi] +
        '?action=history">Authors</a><a href="http://creativecommons.org/licenses/by-sa/3.0/"><img class="cc" src="' +
        p +
        'CC-BY-SA.png" alt="Creative Commons Attribution Share-Alike License"></a></div>\n\
      </div>\n\
      <div class="col-md-6">\n\
        <a href="http://' +
        opt["--link"] +
        '">\n\
          <img src="' +
        p +
        opt["--logo"] +
        '" alt="institution logo">\n\
        </a>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</footer>\n'
    );
    $("#footer").remove();
  },

  wrap: function($) {
    $("#content").addClass("container").wrap('<section id="main"></section>');
    $("#content")
      .wrapInner('<div class="panel-body"/>')
      .wrapInner('<div class="panel"/>')
      .wrapInner('<div class="col-md-12"/>')
      .wrapInner('<div class="row"/>');
  },

  nav: function($, opt, pages, pi, outline, getPrev, getNext) {
    var i,
      l,
      j,
      l2,
      l2start,
      l2end,
      pno,
      actives,
      pinfo,
      psinfo,
      subclasses,
      classes = [];
    var nb =
      '<header>\n\
  <div>\n\
    <div class="container">\n\
      <div class="brandtext">\n\
        <h1><a href="' +
      opt["--urlprefix"] +
      '">' +
      opt["--brand"] +
      '</a></h1>\n\
      </div>\n\
    </div>\n\
  </div>\n\
  <div class="container">\n\
    <nav class="navbar" role="navigation">\n\
      <div class="navbar-header">\n\
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">\n\
          <span class="sr-only">Toggle navigation</span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
        </button>\n\
      </div>\n\
      <div class="collapse navbar-collapse navbar-ex1-collapse">\n\
        <ul class="nav navbar-nav">\n';

    l = outline.length;
    actives = outline[pi].path;
    for (i = 1; i < l; i++) {
      classes = [];
      if (actives.indexOf(i) > -1) {
        classes.push("active");
      }
      pinfo = outline[i];
      if (pinfo.depth === 0) {
        if (pinfo.hasChildren) {
          classes.push("dropdown");
          nb += '          <li class="' + classes.join(" ") + '">\n';
          nb +=
            '            <a href="#" class="dropdown-toggle" data-toggle="dropdown">';
          nb += pinfo.text;
          //nb += ' ' + i;
          nb += '<span class="caret"></span>';
          nb += "</a>\n";
          // submenu here
          nb += '            <ul class="dropdown-menu" role="menu">\n';
          for (j = i + 1; j < l; j++) {
            subclasses = [];
            if (actives.indexOf(j) > -1) {
              subclasses.push("active");
            }
            psinfo = outline[j];
            if (psinfo.depth === 0) {
              break;
            } else if (psinfo.depth === 1) {
              nb += "              <li";
              if (classes.length) {
                nb += ' class="' + subclasses.join(" ") + '"';
              }
              nb += ">";
              var lurl = psinfo.url;
              if (!lurl) {
                lurl = outline[getNext(j, pages)].url;
              }
              if (lurl) {
                nb += '<a href="' + lurl + '">';
                nb += psinfo.text;
                //nb += ' ' + i + ',' + j;
                nb += "</a>";
              } else {
                nb += psinfo.text;
              }
              nb += "              </li>\n";
            }
          }
          nb += "            </ul>\n";
          nb += "          </li>\n";
        } else if (pinfo.url) {
          nb += '          <li><a href="' + pinfo.url + '">';
          nb += pinfo.text;
          nb += "</a></li>\n";
        } else {
          nb += "          <li>" + pinfo.text + "</li>";
        }
      }
    }
    nb +=
      '        </ul>\n\
        <ul class="nav navbar-nav navbar-right">\n\
          <li><a id="weUser" href="#" class="navbar-link"></a></li>\n\
        </ul>\n\
      </div>\n\
    </nav>\n\
  </div>\n\
</header>\n';

    /*
    // third level navbar
    if (!opt['--rthird'] && outline[pi].depth === 2) {
      nb += '<nav class="navbar navbar-default" role="navigation">\n\
            <div class="container-fluid">\n\
              <div class="navbar-header">\n\
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#l2nav">\n\
                  <span class="sr-only">Toggle navigation</span>\n\
                  <span class="glyphicon glyphicon-th-list"></span>\n\
                </button>\n\
              </div>\n\
              <div class="collapse navbar-collapse" id="l2nav">\n\
                <ul class="nav navbar-nav">\n';
      pno = 1;
      l2start = pi;
      while (l2start > 0) {
        if (outline[l2start-1].depth !== 2) {
          break;
        }
        l2start--;
      }
      l2end = pi;
      while (l2end < outline.length) {
        if (outline[l2end].depth !== 2) {
          break;
        }
        l2end++;
      }
      var l2text;
      for (l2=l2start; l2<l2end; l2++) {
        nb += '<li' + ((l2 === pi) ? ' class="active"' : '') + '><a href="' + outline[l2].url;
        //l2text = pno + '';
        l2text = '<span class="glyphicon glyphicon-file"></span>';
        if (opt['--completethird']) {
          nb += '" title="' + outline[l2].text + '">' + outline[l2].text + '</a></li>\n';
        } else {
          nb += '" title="' + outline[l2].text + '">' + (((l2 >= pi-1) && (l2 <= pi+1)) ? outline[l2].text : l2text) + '</a></li>\n';
        }
      }

      nb += '   </ul>\n\
        </div>\n\
      </div>\n\
    </nav>\n';
    }
    */

    $("body").prepend(nb);
  },

  prevnext: function($, pl, nl) {
    var pager =
      '    <div class="row">\n\
      <div class="col-md-12">\n\
        <ul class="pager">\n';
    if (pl) {
      pager +=
        '          <li class="previous">\n\
            <a href="' +
        pl +
        '">&larr; Previous</a>\n\
          </li>\n';
    }
    if (nl) {
      pager +=
        '          <li class="next">\n\
            <a href="' +
        nl +
        '">Next &rarr;</a>\n\
          </li>\n';
    }
    pager += "        </ul>\n\
      </div>\n\
    </div>\n";
    $("#main>div").append(pager);
  },

  process: function($, opt) {
    // map iDevice type to icon
    var icons = {
      activity: "activity",
      activities: "activity",
      aim: "objectives",
      aims: "objectives",
      portfolio_activity: "activity",
      extension_exercise: "activity",
      assignment: "assess",
      example: "casestudy",
      case_study: "casestudy",
      assessment: "qmark",
      self_assessment: "qmark",
      question: "qmark",
      did_you_know: "qmark",
      did_you_notice: "qmark",
      definition: "define",
      definitions: "define",
      discussion: "discussion",
      tell_us_a_story: "discussion",
      objective: "objectives",
      objectives: "objectives",
      outcomes: "objectives",
      key_points: "key_points",
      media_required: "multimedia",
      media: "multimedia",
      reading: "reading",
      competency: "review",
      competencies: "review",
      summary: "summary",
      reflection: "reflection",
      preknowledge: "preknowledge",
      web_resources: "inter"
    };
    $('script[src*="wikieducator.org/load.php"]').remove();
    $("#top").remove();

    // restyle iDevices
    $(".eXe-iDevice").each(function() {
      var classes,
        $tloc,
        $inner,
        body,
        title,
        icon,
        p = opt["--urlprefix"] + "/img/",
        idtype = $(this).data("iDevice");
      if (!idtype) {
        classes = $(this).attr("class").split(" ");
        idtype = classes.filter(function(e) {
          return e !== "eXe-iDevice";
        })[0];
      }

      icon = "activity";
      if (icons.hasOwnProperty(idtype)) {
        icon = icons[idtype];
      }

      // process new style Template:IDevice separately
      if ($(this).hasClass("iDevice")) {
        $(this).removeAttr("style").addClass("panel");
        // remove old icon, add new (works for both line and wikieducator)
        $(this).find(".iDevice-icon").remove();
        $tloc = $(this).find(".iDevice-title");
        $tloc.first().parent().removeAttr("style").addClass("panel-heading");
        $tloc.wrapInner("<h2 />");
        $tloc
          .removeAttr("style")
          .before(
            '<div><img class="pedagogicalicon" alt="' +
              idtype +
              ' icon" src="' +
              p +
              "Icon_" +
              icon +
              '.png"></div>'
          );
        body = $(this)
          .find(".iDevice-body")
          .addClass("panel-body")
          .removeAttr("style");
      } else {
        // there are (at least) three places where the title might be
        $inner = $(this).children("div:eq(0)");
        if ($inner.children("div:eq(1)").length > 0) {
          title = $inner.children("div:eq(1)").text();
          $tloc = $inner.children("div:eq(1)");
        } else if ($inner.find("h1:first").length > 0) {
          title = $inner.find("h1:first").text();
          $tloc = $inner.find("h1:first").parent();
        } else {
          title = $(this).children("div:eq(1)").text();
          $tloc = $(this).children("div:eq(1)");
        }
        title = $.trim(title);
        $(this).children("div").removeAttr("style");
        $(this).removeAttr("style");
        body = $(this).find("table:first tr td").html();
        icon = "qmark";
        if (icons.hasOwnProperty(idtype)) {
          icon = icons[idtype];
        }
        $(this).prepend(
          '<div class="panel-heading">\n\
          <div>\n\
            <img class="pedagogicalicon" src="' +
            p +
            "Icon_" +
            icon +
            '.png">\n\
          </div>\n\
          <div>\n\
            <h2>' +
            title +
            "</h2>\n\
          </div>\n\
        </div>\n" +
            body
        );
        $(this).find("table").remove();
        $tloc.remove();
        $(this).find(".floatleft").remove();
      }
    });
    // restyle boxes
    $(".WEbox").find(".panel-primary:first").removeClass("panel-primary");
    $(".WEbox-header").each(function() {
      var contents = $(this).contents();
      $(this).next().prepend("<h2 />");
      $(this).next().find("h2").append(contents);
      $(this).remove();
    });
    $(".WEbutton").each(function() {
      var contents;
      $(this).find("a").removeAttr("class");
      $(this).removeAttr("style").removeClass("WEbutton").addClass("button");
    });
    // tables
    $("table.oeru1, table.prettytable")
      .addClass("table")
      .addClass("table-striped");
    // images
    if (opt["--wpurl"]) {
      // rewrite links to image pages for attribution/licensing
      $("a.image").each(function() {
        var dst = $(this).attr("href");
        if (dst.indexOf("/File:") === 0) {
          $(this).attr("href", "http://WikiEducator.org" + dst);
        }
      });
      // FIXME images should be uploaded to Wordpress rather than hotlinked
      $("img").each(function() {
        var p,
          dst = $(this).attr("src");
        if ($(this).hasClass("pedagogicalicon")) {
          p = dst.lastIndexOf("/");
          if (p > -1) {
            $(this).attr(
              "src",
              "/wp-content/themes/oeru_course/idevices" + dst.slice(p)
            );
          }
        } else if (
          dst.length >= 2 &&
          dst.charAt(0) === "/" &&
          dst.charAt(1) !== "/"
        ) {
          dst = "//WikiEducator.org" + dst;
          $(this).attr("src", dst);
          // FIXME rewrite sourceset srcs rather than removing them
          $(this).removeAttr("srcset");
        }
      });
    }
    $(".thumbinner").addClass("thumbnail").addClass("img-responsive");
    $("section#main")
      .find("img")
      .not(".pedagogicalicon")
      .addClass("img-responsive");
    // override Bootstrap's img block in Template:Pdf
    $('.pdfdown img[alt="PDF down.png"]').css("display", "inline-block");
  }
};
