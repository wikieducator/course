#!/usr/bin/env node
/* jshint node: true, multistr: true, trailing: true, esnext: true */
"use strict";

const VERSION = '0.1.0';

var docopt = require('docopt').docopt,
    wordpress = require('wordpress');

var wp,
    opt,
    doc = `Usage:
  xmlrpc --wpurl=URL --wpuser=USER --wppass=PASS [--wpmenu]
  xmlrpc -h | --help
  xmlrpc --version

  --wpurl=URL                 WordPress URL.
  --wpuser=USER               WordPress user.
  --wppass=PASS               WordPress password.
  --wpmenu                    Generate menu in newSplashWP theme.
  -h, --help                  Show this help.
  --version                   Display version.
  `;

opt = docopt(doc, {version: VERSION});
console.log(opt);

wp = wordpress.createClient({
  url: opt['--wpurl'],
  username: opt['--wpuser'],
  password: opt['--wppass']
});

// Find out what XMLRPC methods are supported
wp.listMethods(function(error, methods) {
  if (error) {
    console.log(`Unable to retrieve method list: ${error}`);
  } else {
    console.log('XMLRCP methods', methods);
    if (opt['--wpmenu'] && methods.indexOf('oeru_theme.menu_create') > -1) {
      wp.authenticatedCall('oeru_theme.menu_create', function(error, data) {
        if (error) {
          console.log(`Menu creation failed: ${error}`);
        } else {
          console.log('Menu created', data);
        }
      });
    }
  }
});

