#!/usr/bin/env node
/* jshint node: true, multistr: true, trailing: true, esnext: true */
"use strict";

var VERSION = "0.7.3",
  JQUERY = "http://code.jquery.com/jquery-2.1.1.min.js",
  FILEPATH = "/tmp"; // should be '/home/www/www' on WE

var jsdom = require("jsdom"),
  docopt = require("docopt").docopt,
  fs = require("fs"),
  mkdirp = require("mkdirp"),
  path = require("path"),
  wordpress = require("wordpress"),
  theme;

var pages = [], // list of page URLs to be processed
  outline = [], // list of maps of page info
  // outline, map per page
  // text   text title of page
  // wkpage wiki page name
  // url    fragment, typically starting "/course/"
  // depth  depth in tree
  // hasChildren    boolean has children
  // node   index number
  // path   array of outline indexes,
  //          starting at root down to this item
  // wpID   wp node id number
  urls = {}, // map of original URLs to course URLs
  parent_ids = {}, // map from URL to wp page id
  pi = 0, // process index
  base_url = "http://wikieducator.org", // no trailing slash
  wp,
  opt,
  doc = `Usage:
  course [options] OUTLINE PAGEPREFIX
  course -h | --help
  course --version

Options:
  -b BRAND, --brand=BRAND     Menubar brand [default: OCL4Ed].
  -l LOGO, --logo=LOGO        Institution logo
  -k LOGOLINK, --link=LOGOLINK Institution link
  -t THEME, --theme=THEME     Desired theme [default: default].
  -u URLPREFIX, --urlprefix=URLPREFIX  Filesystem URL component.
  -p, --prevnext              Include previous/next links.
  -s, --scan                  Include SCAN page.
  -r, --rthird                Exclude third level navigation.
  -c, --completethird         Include complete third level titles.
  --siteid=SITEID             OERu Piwik site id.
  --wpurl=URL                 Wordpress URL.
  --wpuser=USER               Wordpress user.
  --wppass=PASS               Wordpress password.
  --wpmenu                    Generate menu in newSplashWP theme.
  --debugout                  Show content to be written.
  -h, --help                  Show this help.
  --version                   Display version.
  `;

opt = docopt(doc, { version: VERSION });
console.log(opt);
if (opt["--theme"]) {
  theme = require("./themes/" + opt["--theme"]);
}
if (opt["--link"] && !/^http/i.test(opt["--link"])) {
  opt["--link"] = "http://" + opt["--link"];
}
if (opt["--logo"] && !/^http/i.test(opt["--logo"])) {
  opt["--logo"] = opt["--urlprefix"] + "/img/" + opt["--logo"];
}
if (opt["--wpurl"]) {
  wp = wordpress.createClient({
    url: opt["--wpurl"],
    username: opt["--wpuser"],
    password: opt["--wppass"]
  });
  opt["--urlprefix"] = "";
  getOutline();
} else {
  if (opt["--urlprefix"] && opt["--urlprefix"].indexOf("/course") !== 0) {
    opt["--urlprefix"] = "/course" + opt["--urlprefix"];
  }
  getOutline();
}

function pageToURL(p) {
  p = p
    .replace(/%../g, "")
    .replace(/ - /g, "-")
    .replace(/[ _:;]/g, "-")
    .replace(/[^-/a-zA-Z0-9]/g, "");
  return opt["--urlprefix"] + p;
}

function courseURLToFile(u) {
  return FILEPATH + pageToURL(u);
}

// FIXME apply real Wordpress slug conversion rules
function slugify(u) {
  return u
    .replace(opt.PAGEPREFIX, "")
    .toLowerCase()
    .replace(/ - /g, "-")
    .replace(/[ _]/g, "-")
    .replace(/[()?]/g, "");
}

// step through pages array looking for prev/next non-blank page
function getPrev(pi, pages) {
  while (--pi >= 0) {
    if (pages[pi] !== "") {
      return pi;
    }
  }
  return -1;
}
function getNext(pi, pages) {
  var l = pages.length;
  while (++pi <= pages.length) {
    if (pages[pi] !== "") {
      return pi;
    }
  }
  return l;
}

function scanPage() {
  var sp;
  if (!opt["--scan"]) {
    return "";
  }
  sp = `<div id="scanpage" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <img alt="OERu logo" src="' + opt['--urlprefix'] + '/img/OERu-46x46.png" width="46" height="46" border="0" style="float: left; margin-right: 10px;">
        <h4 class="modal-title">' + opt['--brand'] + '</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <a href="' + opt['--urlprefix'] + '/About/" class="btn btn-large btn-primary center-block">Open Content Licensing for Educators<br>Course overview</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Course_guide/Aims/" class="btn btn-large btn-primary center-block">Course aims</a>
          </div>
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Course_guide/" class="btn btn-large btn-primary center-block">Course guide</a>
          </div>
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Assessment/" class="btn btn-large btn-primary center-block">Assessment</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <a href="' + opt['--urlprefix'] + '/Start_here/" class="btn btn-large btn-info center-block">Start here</a>
          </div>
          <div class="col-md-6">
            <a href="' + opt['--urlprefix'] + '/Registration/" class="btn btn-large btn-info center-block">Log in</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Console/" class="btn btn-large btn-primary center-block">Learning console</a>
          </div>
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Console/#Announcements" class="btn btn-large btn-primary center-block">Announcements</a>
          </div>
          <div class="col-md-4">
            <a href="' + opt['--urlprefix'] + '/Support/" class="btn btn-large btn-primary center-block">Support</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <a href="' + opt['--urlprefix'] + '/Console/#Course_schedule" class="btn btn-large btn-primary center-block">Recommended course schedule</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>`;
  return sp;
}

/* build navBar for current page */
function navBar(pi) {}

function processPage(pi) {
  var px = pi,
    url = pages[pi];
  // if non-existent page... duplicate next one in Wordpress mode
  while (url === "" && ++px < pages.length) {
    url = pages[px];
  }

  jsdom.env(base_url + url, [JQUERY], function(errors, window) {
    var page,
      pagename,
      filename,
      $ = window.$;
    $("title").text($("title").text().replace(" - WikiEducator", ""));
    // FIXME hack for Practice namespace course
    $("title").text($("title").text().replace("Practice:", ""));
    // check for legacy navigation
    var $legacynav = $("table.navigation");
    var $legacytitle,
      legacytitle = "";
    if ($legacynav.length && $legacynav.hasClass("collapsible")) {
      legacytitle = $("#firstHeading").text();
      if (legacytitle === "") {
        $legacytitle = $legacynav.find("strong.selflink:first");
        if ($legacytitle.length) {
          legacytitle = $legacytitle.text();
        }
      }
    }
    // remove some things
    $(
      `.weMenubar,
         #mw-head,
         #mw-page-base,
         #mw-head-base,
         #globalWrapper,
         #firstHeading,
         #mw-content-text h1:first,
         .weCourseTitle,
         .navigation,
         .magnify,
         .mw-editsection,
         #mw-navigation,
         #mw-js-message,
         #mw-panel,
         #siteNotice,
         #siteSub,
         #jump-to-nav,
         #toc,
         #contentSub,
         #catlinks,
         #footer-places-disclaimer,
         #footer-places-about,
         #footer-info-lastmod,
         #footer-info-viewcount,
         #footer-poweredbyico`
    ).remove();

    // rework a ul list-group into a div
    $("ul.list-group").each(function(i, e) {
      $(this).find("li").each(function(i, e) {
        var cl = $(this).attr("class");
        if (cl.indexOf("weci-") === 0) {
          $(this)
            .find("a:first")
            .prepend(
              '<span class="glyphicon glyphicon-' +
                cl.slice(5) +
                '"></span>&nbsp;'
            )
            .addClass("list-group-item")
            .unwrap();
        }
        var libody = $(this).html();
      });
      var lgbody = '<div class="list-group">' + $(this).html() + "</div>";
      $(this).replaceWith(lgbody);
    });
    $(".list-group")
      .wrap('<div class="row"></div>')
      .wrap('<div class="col-md-4"></div>');

    theme.nav($, opt, pages, pi, outline, getPrev, getNext);

    // rejig the URLs we are moving
    $("a").each(function(i, e) {
      var orig = $(this).attr("href");
      if (urls.hasOwnProperty(orig)) {
        $(this).attr("href", urls[orig]);
      } else {
        // other WikiEducator relative pages that are not in the outline
        if (orig && orig.indexOf("//") === -1) {
          $(this).attr("href", "//wikieducator.org" + orig);
        }
      }
    });

    // embed YouTube videos
    $(".weYouTube").each(function(i, e) {
      var $this = $(this),
        id = $this.attr("data-ytid"),
        width = $this.css("width") || 560,
        height = $this.css("height") || 315,
        list = $this.attr("data-list"),
        index = $this.attr("data-index");
      $this
        .parent()
        .replaceWith(
          '<iframe width="' +
            width +
            '" height="' +
            height +
            '" src="//www.youtube.com/embed/' +
            id +
            "?" +
            (list ? "list=" + list : "rel=0") +
            (index ? "&index=" + index : "") +
            '" frameborder="0" allowfullscreen></iframe>'
        );
    });
    // embed SoundCloud clips
    $(".weSoundCloud").each(function(i, e) {
      var $this = $(this),
        embed = $this.find(".embedcode").text(),
        link = $this.find("a:first").attr("href"),
        size = $this.attr("data-height") || $this.attr("data-width") || "450",
        autoplay = $this.attr("data-autoplay") || "false";
      // if the embedcode exists, use it in preference to template params
      if (embed) {
        embed = embed.replace(/&gt;/g, ">").replace(/&lt;/g, "<");
        $this.html(embed);
      } else {
        autoplay = autoplay.toLowerCase();
        if ($.inArray(autoplay, ["true", "false"]) === -1) {
          autoplay = false;
        }
        $this.html(
          '<iframe width="100%" height="' +
            size +
            '" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=' +
            encodeURIComponent(link) +
            "&amp;auto_play=" +
            autoplay +
            '&amp;hide_related=true&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>'
        );
      }
    });
    // if we don't have an h1, but we do have a legacytitle
    // then insert it as an h1
    if ($("h1>.mw-headline").length === 0 && legacytitle) {
      console.log("!!!! legacy title inserted", legacytitle);
      $("#bodyContent").prepend(
        '<h1><span class="mw-headline">' + legacytitle + "</span></h1>\n"
      );
    }

    // make stylesheet & Javascript references point to WikiEducator
    $("head").find('link[rel="stylesheet"]').each(function() {
      var href = $(this).attr("href");
      if (
        href &&
        href.length >= 2 &&
        href.charAt(0) === "/" &&
        href.charAt(1) !== "/"
      ) {
        $(this).attr("href", "http://wikieducator.org" + href);
      }
    });
    $("html").find("script[src]").each(function() {
      var href = $(this).attr("src");
      if (
        href &&
        href.length >= 2 &&
        href.charAt(0) === "/" &&
        href.charAt(1) !== "/"
      ) {
        $(this).attr("src", "http://wikieducator.org" + href);
      }
    });
    // add user fetch
    $('meta[charset="UTF-8"]').after(`
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script>
(function() {
  function gotUser() {
    var userinfo,
        r = JSON.parse(this.responseText);
    if (r && r.query && r.query.userinfo) {
      userinfo = r.query.userinfo;
      if (! userinfo.hasOwnProperty("anon")) {
        window.wgUserName2 = userinfo.name;
        window.wgUserRealName = userinfo.realname;
      }
      window.wgUserLoading = 2;
    } else {
      window.wgUserLoading = -1;
    }
    if (window.wgUserCallback) {
      window.wgUserCallback();
    }
  }

  var req = new XMLHttpRequest();
  window.wgUserLoading = 1;
  req.onload = gotUser;
  req.open("get", "http://wikieducator.org/api.php?action=query&meta=userinfo&uiprop=realname&format=json");
  req.send();
}());
</script>`);

    theme.header($, opt);

    theme.footer($, opt, pages, pi, outline[pi]);

    theme.wrap($);

    if (opt["--prevnext"]) {
      var pl, nl;
      var pp = getPrev(pi, pages);
      if (pp >= 0) {
        pl = urls[pages[pp]];
      }
      var np = getNext(pi, pages);
      if (np < pages.length) {
        nl = urls[pages[np]];
      }
      theme.prevnext($, pl, nl);
    }

    // turn WEboxes into columns
    $(".row").each(function() {
      var $boxes = $(this).children(".WEbox"),
        w;
      if ($boxes.length) {
        w = Math.floor(12 / $boxes.length);
        $boxes.addClass("col-md-" + w);
      }
    });
    // turn WEbutton into button
    $("div.WEbutton a").addClass("btn btn-primary");
    // turn WEbox with WEbox-header into a panel
    $("div.WEbox").each(function() {
      var $header,
        $WEboxHeader = $(this).children("div.WEbox-header").first();
      if ($WEboxHeader.length === 1) {
        $WEboxHeader.addClass("panel-heading");
        $header = $WEboxHeader.detach();
        $(this).wrapInner('<div class="panel-body"></div>');
        $(this).wrapInner('<div class="panel panel-primary"></div>');
        $(this).find("div.panel:first").prepend($header);
      }
    });
    // blank page
    $(".noarticletext").html(
      '<p class="noarticle">This page is currently blank.</p>'
    );
    // change the CC-BY* logos
    $("#footer-copyrightico img").each(function() {
      var src = $(this).attr("src");
      if (src.indexOf("cc-by.png") >= 0) {
        $(this).attr("src", opt["--urlprefix"] + "/img/CC-BY.png");
      } else {
        $(this).attr("src", opt["--urlprefix"] + "/img/CC-BY-SA.png");
      }
    });
    // make collapsible panels collapsible/openable
    $(".panel").each(function(ix) {
      var open,
        toggle_wrapper,
        $pc = $(this).children(".panel-collapse:first");
      // if this contains a collapsible body
      if ($pc.length) {
        // get rid of any anchor A
        $(this).children(".panel-heading").find("a").remove();

        open = $pc.hasClass("in");
        toggle_wrapper =
          '<a data-toggle="collapse" data-target="#collapse' + ix + '" ';
        toggle_wrapper += 'href="#collapse' + ix + '" ';
        if (!open) {
          toggle_wrapper += 'class="collapsed"';
        }
        toggle_wrapper += "></a>";
        $(this).find("h4").wrapInner(toggle_wrapper);
        $pc.attr("id", "collapse" + ix);
      }
    });

    // FIXME wiggio breaks the back button, so hack links to open
    // in a new window
    $('a[href^="http://cd4oe.wiggio.com"]').attr("target", "wiggio");

    theme.process($, opt);

    // if using Piwik, install it at the bottom of the page
    if (opt["--siteid"]) {
      $("body").append(`
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.oeru.org/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', ${opt["--siteid"]}]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//stats.oeru.org/piwik.php?idsite=${opt[
        "--siteid"
      ]}" style="border:0;" alt="" /></p></noscript>
`);
    }

    // save the page: to Wordpress or to filesystem
    if (opt["--wpurl"]) {
      // hotlink to the images on WikiEducator
      $("img").each(function() {
        var src = $(this).attr("src");
        if (
          src.indexOf("http") !== 0 &&
          src.indexOf("//") !== 0 &&
          src.indexOf("/wp") !== 0
        ) {
          $(this).attr("src", "http://WikiEducator.org" + src);
        }
      });
      $("script").remove();

      page = $('<div id="weinwp"/>')
        .append($("#content"))
        .append($("footer"))
        .html();
      pagename = path.basename(outline[pi].text).replace("Practice:OCL4Ed", "");
      console.log(`pagename=${pagename}`);
      console.log(outline[pi]);
      if (pagename === "") {
        setImmediate(processPages);
        return;
      }
      var page_desc = {
        title: outline[pi].text,
        status: "publish",
        type: "page",
        //name: 'page' + outline[pi].node,
        menuOrder: outline[pi].node * 100,
        content: page
      };
      if (outline[pi].path.length >= 2) {
        var myParent = outline[pi].path.slice(-2, -1);
        page_desc.parent = outline[myParent].wpID;
      }
      //console.log('page_desc', page_desc);
      if (opt["--debugout"]) {
        console.log("######");
        console.log(page);
        console.log("######");
      }
      try {
        wp.newPost(page_desc, function(err, data) {
          if (err) {
            console.log(pi, pages[pi], err);
          } else {
            console.log(pi, "wp saved:", data);
            outline[pi].wpID = data;
          }
          setImmediate(processPages);
        });
      } catch (e) {
        console.log(pi, "!!!error writing to wp");
        console.log("------");
        console.log(outline[pi]);
        console.log("------");
        console.log("page_desc", page_desc);
        setImmediate(processPages);
      }
    } else {
      page = "<!DOCTYPE html>\n" + $("<html/>").append($("html")).html();
      page = page.replace(/<script class="jsdom".*ript>/, "");

      pagename = pages[pi];
      filename = courseURLToFile(pagename);
      console.log("pagename:", pagename);
      console.log("  filename:", filename);
      mkdirp.sync(filename);
      fs.writeFileSync(filename + "/index.html", page);
      setImmediate(processPages);
    }
  });
}

function processPages() {
  console.log(`---- processPages(pi=${pi}) ----`);
  if (pi >= pages.length) {
    if (opt["--wpmenu"]) {
      wp.authenticatedCall(
        "wp.setOptions",
        {
          show_on_front: "page",
          page_on_front: outline[0].wpID,
          oeru_theme_menu_create: 1
        },
        function(err, a) {
          if (err) {
            console.log("theme menu create error", err);
          } else {
            console.log("theme menu create", a);
          }
        }
      );
    }
    return;
  }
  // don't skip empty pages in Wordpress mode
  if (pages[pi] || opt["--wpurl"]) {
    processPage(pi++);
  } else {
    pi++;
    setImmediate(processPages);
  }
}

// read the outline
function getOutline() {
  jsdom.env(opt.OUTLINE, [JQUERY], function(errors, window) {
    var $ = window.$,
      $nav;

    // process outline levels recursively
    function readOutline($e, depth, nn, path, baseurl) {
      $e.children("li").each(function() {
        var i,
          $sub,
          mo,
          wkpage,
          text,
          url = "",
          hasPage,
          info = {},
          linked = false,
          $a = $(this).children("a:first");

        if ($a.length) {
          // item has link to a page or redlink
          wkpage = $a.attr("href");
          text = $a.text();
          linked = true;
          if (wkpage.indexOf("redlink=1") > -1) {
            mo = /title=([^&]+)/.exec(wkpage);
            if (mo.length > 1) {
              wkpage = "/" + mo[1];
            }
            linked = false;
          }
          pages.push(wkpage);
          if (opt["--wpurl"]) {
            url = slugify(text);
            if (path) {
              url = baseurl + "/" + url;
            }
          } else {
            url = pageToURL(wkpage);
          }
          urls[wkpage] = url;
          hasPage = true;
        } else {
          // item has no link, so not a real page
          wkpage = $(this).contents().eq(0).text().trim();
          text = wkpage;
          if (opt["--wpurl"]) {
            url = slugify(text);
            if (path) {
              url = baseurl + "/" + url;
            }
          } else {
            url = pageToURL(wkpage);
          }
          hasPage = false;
          pages.push("");
        }

        $sub = $(this).children("ul");
        info.text = text;
        info.wkpage = wkpage;
        info.url = url;
        info.depth = depth;
        info.hasPage = hasPage;
        info.hasChildren = $sub.length > 0;
        info.node = nn;
        info.path = path.slice(0);
        info.path[depth] = nn;
        outline.push(info);
        nn++;

        if ($sub.length) {
          nn = readOutline($sub, depth + 1, nn, info.path, url);
        }
      });
      return nn;
    }

    if (errors) {
      console.log("error reading outline: ", errors);
      return;
    }

    $nav = $("#mw-content-text>ul");
    readOutline($nav, 0, 0, [], "");

    console.log("==== Outline ====", outline.length);
    outline.forEach(function(v, i) {
      console.log(v);
    });

    console.log("==== URLs ====");
    console.log(urls);

    console.log("==== Pages ====", pages.length);
    pages.forEach(function(v, i) {
      console.log(i, v);
    });

    if (opt["--wpurl"]) {
      // make the first page the name of the course
      let title = opt.PAGEPREFIX.replace(/\//g, "").replace(/_/g, " ");
      urls[pages[0]] = "/" + slugify(title);
      outline[0].url = "/" + slugify(title);
      outline[0].text = title;
    }
    setImmediate(processPages);
  });
}
